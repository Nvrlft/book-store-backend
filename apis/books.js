var express = require ('express')
var router = express.Router()
const mongodb = require('mongodb');
const ObjectID = require ('mongodb').ObjectID;

const DB_Name = 'book_store';
const BOOK_COLLECTION_NAME = "Books";

const DB_URI = 'mongodb://localhost:27017'
const Mongoclient = mongodb.MongoClient;
const client = new Mongoclient(DB_URI, {useNewUrlParser: true, useUnifiedTopology: true});

//endpoint get request
 router.get('/', function(req, res) {
     client.connect(function(err, connection){
       const db = connection.db(DB_Name);//connecting to the bookstore database
       db.collection(BOOK_COLLECTION_NAME)
         .find({})
         .toArray(function(find_err, records){
           if (find_err)
             return res.status(500).send({error: find_err});

             return res.send(records);
         });
       });
})

router.post('/', function (req, res){
    if (!req.body || req.body.length === 0)//if it is null used did not send any data
      return res.status(400).send({message: "Record is required."})

      console.log(req.body);

      //data validation
      if (!req.body.title || !req.body.author || !req.body.price)
        return res.status(400).send({
          message: "Title, author, price ... are required"
        });

    //data is in req.body
    client.connect(function(err, connection){
      const db = connection.db(DB_Name);

      db.collection(BOOK_COLLECTION_NAME)
        .insertOne(req.body,function(insert_error, data){
          if (insert_error)
            return res.status(500).send({message: "Something went wrong"});

            if(connection)
            return res.status(200).send({message: "Book added."});
            connection.close();
        });

      });
})

router.put('/:id', function(req, res){
// localhost:5050/12345
  client.connect(function(err, connection){
    if (err)
      res.status(500).send({error: err});

      console.log(req.body);

      const db = connection.db(DB_Name)
      db.collection(BOOK_COLLECTION_NAME)
        .updateOne({_id:ObjectID (req.params.id)},{$set: req.body}, function(update_err, update_data){
          if (update_err)
            res.status(500).send({error: update_err, message: "Could not update the record"});

            if (update_data)
            return res.status(200).send({message: "Update was successful!"});
        });


  });

});

router.delete('/:id', function (req, res){
  if (!req.body || req.body.length === 0)//if it is null used did not send any data
    return res.status(400).send({message: "Record is required."})

    console.log(req.body);
    
  //data is in req.body
  client.connect(function(err, connection){
    const db = connection.db(DB_Name);

    db.collection(BOOK_COLLECTION_NAME)
      .deleteOne(req.body,function(remove_error, data){
        if (remove_error)
          return res.status(500).send({message: "Something went wrong"});

          connection.close();

          return res.status(200).send(data)
      });

    });
})

module.exports = router
