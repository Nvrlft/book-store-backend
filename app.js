const express = require('express');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const ObjectID = require ('mongodb').ObjectID;

const books = require('./apis/books');
const users = require('./apis/users');
const orders = require('./apis/orders');

const app = express();
const PORT = 5050;
const DB_URI = 'http//:localhost:5050';

app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Content-Type");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  next();
});

app.use(bodyParser.json());//allow user to send JSON data
app.use(bodyParser.urlencoded({ extended: false}))//allow user to send data withing the url

app.use('/books', books);
app.use('/users', users);
app.use('/orders', orders);


app.listen(PORT)
console.log("Listening on port " + PORT);
